import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int NumberOFelement = 0;
        int arr[];
        int newArr[];
        int newNumberOFelement = 0;
        System.out.print("Please input number of element: ");;
        NumberOFelement = sc.nextInt();
        arr = new int[NumberOFelement];
        newArr = new int[NumberOFelement];
        for(int i = 0; i < arr.length; i++) {
            System.out.print("Element " + i + ": ");
            arr[i] = sc.nextInt();
            int index = -1;
            for(int j = 0; j < newNumberOFelement; j++) {
                if(newArr[j] == arr[i]) {
                    index = j;
                }
            }
            if(index < 0) {
                newArr[newNumberOFelement] = arr[i];
                newNumberOFelement++;
            }
        }
        System.out.print("All number: ");
        for(int i = 0; i < newNumberOFelement; i++) {
            System.out.print(newArr[i] + " ");
        }
    }
}
